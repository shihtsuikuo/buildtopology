package bean;

import java.util.Comparator;

public class CandidateCompare implements Comparator<Candidate>{

	@Override
	public int compare(Candidate c1, Candidate c2) {
		if(c1.getDistance() > c2.getDistance())
			return 1;
		if(c1.getDistance() < c2.getDistance())
			return -1;
		
		return 0;
	}

}
