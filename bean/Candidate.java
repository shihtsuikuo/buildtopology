package bean;

public class Candidate {
	/**
	 * Actually, appID is clusterID here. That is, on/off for the same appliance would be two different ID number.
	 */
	private int appID;
	/**
	 * KNN distance reported from meter.
	 * Only when KNN distance < 0.5 will the program recognize its corresponding appliance as a valuable answer.
	 */
	private float distance;
	
	public int getAppID(){
		return appID;
	}
	public float getDistance(){
		return distance;
	}
	
	public void setAppID(int v){
		appID = v;
	}
	public void setDistance(float f){
		distance = f;
	}
}
