package bean;

import java.sql.Timestamp;

public class Event {
	static int poolSize = 10; // appliance number * 2
	/**
	 * dirtyBit : mark as true if ILP make a different decision
	 */
	public boolean dirtyBit;
	public int EventID;
	public String Timestamp;
	public float timestampFloat;
	public Timestamp timestampDate;
	public float V, I, P, Q;
	//public float apparent_power;
	//public float active_power;
	public int recognizedAppliance;
	/**
	 * Indicate who is on duty after this event.
	 * Here the appliances are shown as cluster ID.
	 */
	public boolean[] appState = new boolean[poolSize];
	/**
	 * power factor
	 */
	public float pf;
	public Candidate[] appCandidate = new Candidate[poolSize];
	
	public Event(){
		int EventID = -1;
		V = 0.0F;
		I = 0.0F;
		P = 0.0F;
		Q = 0.0F;
		pf = 0.0F;
		dirtyBit = false;
		for(int i = 0; i < poolSize; i++)
			appState[i] = false;
	}
	
	public void setRecognizedAppliance(){
		int answer = 0; 
		for(int i = 0; i < poolSize; i++){
			if(appCandidate[i].getDistance() < appCandidate[answer].getDistance()) answer = i;
		}
		recognizedAppliance = answer;
	}
	
//	public void sortCandidate(){
//		Arrays.sort(appCandidate, new CandidateCompare());
//	}
}
