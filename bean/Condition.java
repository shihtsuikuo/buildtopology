package bean;

import java.util.Vector;

/**
 * 
 * @author Ariel_K
 * Condition is 
 */
public class Condition {
	/**
	 * Indicate who is on duty.
	 * Here the appliances are shown as cluster ID.
	 */
	//public int[] appState = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0};   //////////////poolsize=10
	/**
	 * The event signature ID for this condition.
	 */
	public Vector<Integer> eID = new Vector<Integer>();
	/**
	 * The sum of the appState, to get the # of "ON" appliance instantly.
	 */
	//public int appStateSum = 0;
	
	//------------------------------------------------------------------------------------------
	
	/**
	 * index: precondition clusterID, value: support.
	 */
	public int[] precondition = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0};  ///////////////poolsize=10
}
