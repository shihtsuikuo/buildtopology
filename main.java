import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Vector;

import lpsolve.*;
import tool.DBConnection;
import tool.Debugger;
import tool.Impedance;
import tool.Rectangular;
import tool.Resistance;
import bean.Candidate;
import bean.Condition;
import bean.ConditionCompare;
import bean.Event;


public class main {
	private static final int AVERAGE_ENTRIES = 3;
	private static final int EVENT_PADDING = 2;    // the event-occur time are usually delay captured, thus when calculating resistance should skip some entries.
	private static final float DISTANCE_THRESHOLD = 0.2F;
	private static final int LP_WINDOW_SIZE = 1;
	private static final int PRECOND_WINDOW_SIZE = 5;
	private static final int FREQUENCY = 60;
	/**
	 * DUMMY_SIZE the the number of dummy ILP variable.
	 * The number of DUMMY_SIZE should be two times as the number of appliance in the topology. (same as poolSize)
	 * The dummy variables are set to allow some precondition before the whole sequence start. 
	 * That is, we get apply this model to any non-complete sequence.
	 */
	private static final int DUMMY_SIZE = 0;
	/**
	 * poolSize indicates the number of appliances in the pool.
	 * With the test data on April, 2014, the number of appliances is 5
	 * appID 1 on -> clusterID = 1, appID 1 off -> clusterID = 2. 
	 */
	final static int poolSize = 10;
	final static int[] currentAppCondition = new int[poolSize];
	final static float[][] distanceMatrix = new float[poolSize][poolSize];
	final static Vector<Event> events = new Vector<Event>();
	/** label the events which are switched on, when there is no other appliance working */
	final static Vector<Integer> alone_label = new Vector<Integer>();
	//final static Condition[] condLog = new Condition[poolSize];
	public static Connection conn = null;
	public static Statement statement1 = null;
	public static ResultSet rs = null;
	public static void main(String[] args) {
		
		//for(int i = 0; i < poolSize; i++) condLog[i] = new Condition();
		/******************************************************* Get Sequence ***************************************************************/
		try {

				conn = DBConnection.getConnection();
				statement1 = conn.createStatement();
				String queryAppID = "select * from APP_Topology1_Sequence3";// LIMIT 0 , 200"; //get event sequence
				rs = statement1.executeQuery(queryAppID);
				
				while (rs.next()) {
					Event newEvent = new Event();
					newEvent.EventID = rs.getInt("Idx");
					newEvent.timestampFloat = rs.getFloat("T");
//					newEvent.timestampDate = rs.getTimestamp("timestamp");
//					newEvent.timestampLong = newEvent.timestampDate.getTime();
					newEvent.I = rs.getFloat("I");
					newEvent.V = rs.getFloat("V");
					newEvent.P = rs.getFloat("P");
					newEvent.Q = rs.getFloat("Q");
					//newEvent.pf = rs.getFloat("power_fac");
					for(int c = 1; c <= poolSize; c++){
						Candidate tempCand = new Candidate();
						tempCand.setAppID(c);
						if(c%2 == 1)
							tempCand.setDistance(rs.getFloat("D"+(c+1)));
						else
							tempCand.setDistance(rs.getFloat("D"+(c-1)));
						newEvent.appCandidate[c-1] = tempCand;
					}
					/* Sort Candidates according to distance*/
					newEvent.setRecognizedAppliance();
//					newEvent.sortCandidate();
					//System.out.print(newEvent.appCandidate[0].getAppID() + " " + newEvent.appCandidate[0].getDistance());
					events.add(newEvent);

				}
				rs.close();
				statement1.close();
			
		} catch (SQLException sqlEx) {
			System.out.println("DB connection fail.");
			// log connection fail.
		} catch (Exception e1) {
			
			e1.printStackTrace();
		}
		
		/**************************************   Linear Programming - Check Sequence   *******************************************************/
//		int originEventSize = events.size();
//		for(int duplicateTime = 1; duplicateTime < 6; duplicateTime++){
//			for(int i = 0; i< originEventSize; i++) events.add(events.elementAt(i));
//		}
//		System.out.println(events.size());
		int correction = 0;
		try {
		      // Create a problem with _____ variables and 0 constraints
			  int variableSize = events.size() * poolSize + DUMMY_SIZE;
		      LpSolve solver = LpSolve.makeLp(0, variableSize);
			  if(DUMMY_SIZE != 0){
				  for(int i = 0; i < DUMMY_SIZE; i++){
			    	  solver.setBinary(i+1, true);
			    	  if(i % 2 == 0)
			    		  solver.setColName(i+1, "PRE_ON _" + ((i/2) + 1));
			    	  else
			    		  solver.setColName(i+1,  "PRE_OFF_" + ((i/2) + 1));
			    	  
			      }
			  }
		      for(int i = DUMMY_SIZE; i < variableSize; i++){
		    	  int i_minus_dummy = i - DUMMY_SIZE;
		    	  solver.setBinary(i+1, true);   // Happened >> 1; did not happen >>0. For both ON and OFF.
		    	  if((i_minus_dummy) % 2 == 0) 
		    		  solver.setColName(i+1, "ON _" + (i_minus_dummy / poolSize) + "_" +((i_minus_dummy % poolSize)+1) );
		    	  else 
		    		  solver.setColName(i+1, "OFF_" + (i_minus_dummy / poolSize) + "_" + ((i_minus_dummy % poolSize)+1) );
		      }
		    
		      // add constraints

		      /**    1*ON(i) + 1*OFF(i) <= 1   **/
		      for(int i = 0; i < variableSize; i+=2){ 
		    	  double[] constr = { 1, 1 };
		    	  int[] colno = { i+1, i+2 };
		    	  solver.addConstraintex(2, constr, colno, LpSolve.LE, 1);
		      }
		      
		      /**   for each event, just one appliance can switch ON/OFF (others stay)  **/
		      for(int i = 0; i < events.size(); i++){
		    	  double[] constr = new double[poolSize];
		    	  int[] colno = new int[poolSize];
		    	  for(int idx = 0; idx < poolSize; idx++){
		    		  constr[idx] = 1;
		    		  colno[idx] = (i * poolSize + idx + 1) + DUMMY_SIZE;
		    	  }
		    	  solver.addConstraintex(poolSize, constr, colno, LpSolve.EQ, 1);
		      }
		      /**    for each appliance, at any time, the OFF event cannot be more than ON event   **/
		      for(int n = 0; n < poolSize/2; n++){
		    	  for(int idx = 0; idx < events.size() + (DUMMY_SIZE / poolSize); idx++){
	    			  double[] constr = new double[2*(idx+1)];
			    	  int[] colno = new int[2*(idx+1)];
			    	  
		    		  for(int idx2 = 0; idx2 <= idx; idx2++){
		    			  constr[idx2*2] = 1; // coefficient of "ON" variables
		    			  constr[idx2*2+1] = -1; //coefficient of  "OFF" variables
		    			  colno[idx2*2] = poolSize*idx2 + n*2 + 1;  // variable index of "ON"
		    			  colno[idx2*2+1] = poolSize*idx2 + n*2 + 2;  //variable index of "OFF"
		    		  }
		    		  solver.addConstraintex(2*(idx+1), constr, colno, LpSolve.GE, 0);
		    		  solver.addConstraintex(2*(idx+1), constr, colno, LpSolve.LE, 1);
		    	  }
		      }
		      // set objective function
		      double[] objCoeff = new double[variableSize+1];
		      objCoeff[0] = 0;
		      for(int i = 0; i < events.size() ; i++){
		    	  for(int j = 0; j < poolSize; j++){
		    		  objCoeff[(i + (DUMMY_SIZE / poolSize))*poolSize + j + 1] = events.elementAt(i).appCandidate[j].getDistance();
		    	  }
		      }
		      solver.setObjFn(objCoeff);

		      solver.setMinim();
		      
		      // solve the problem
		      solver.solve();

		      // print solution & mark event
		      System.out.println("\nValue of objective function: " + solver.getObjective());
		      double[] var = solver.getPtrVariables();		      
		      for(int i = 0; i< DUMMY_SIZE; i++)
		    	  System.out.println(solver.getColName(i + 1) + ": " + var[i]);
		      
		      // stored events index start from 0
		      // stored appliance cluster# start from 0. 
		      // (i%poolSize+1) is output appliance cluster#
		      // (i/poolSize-1) is event index
		      for (int i = DUMMY_SIZE; i < var.length; i++) {
		    	if(var[i]==1){
		    		int origin_answer = events.elementAt( (i/poolSize)-(DUMMY_SIZE/poolSize) ).recognizedAppliance;   
		    		System.out.print( (origin_answer+1) + " " + ((i%poolSize)+1));                
		    		if( origin_answer != (i % poolSize) ){ 
		    			System.out.print("  v");  
		    			correction++;
		    			events.elementAt((i/poolSize)-(DUMMY_SIZE/poolSize)).recognizedAppliance = (i % poolSize);
		    			events.elementAt((i/poolSize)-(DUMMY_SIZE/poolSize)).dirtyBit = true;
		    		}
		    		
		    	}
		    	if((i+1)%poolSize == 0) System.out.println();
		      }

		      // delete the problem and free memory
		      solver.deleteLp();
		    }
		    catch (LpSolveException e2) {
		       e2.printStackTrace();
		    }
			System.out.println("correction: " + correction);
		/*********************************** label Preconditions, and save preconditions for each event *************************************/
		int e = 0;
		while(e < events.size()){
			int currentClusterID = -1;
			if(e-1 >= 0){
				System.arraycopy(events.elementAt(e-1).appState, 0, events.elementAt(e).appState, 0, poolSize);
			}
			/* check precondition  */
			boolean ALL_OFF = true;
			for(int i = 0; i < poolSize; i++){
				ALL_OFF &= (!events.elementAt(e).appState[i]); 
				if(ALL_OFF == false) break;
			}
			if(ALL_OFF == true){
				alone_label.add(e);
			}
			/*  update appState */
			//the appID without distance threshold check is clusterID, also, it could be a ON event, or a OFF event
			currentClusterID = events.elementAt(e).recognizedAppliance;     
			if(currentClusterID % 2 == 0){                                //ON EVENT
				events.elementAt(e).appState[currentClusterID/2] = true;
			}else{                                                        //OFF EVENT
				events.elementAt(e).appState[currentClusterID/2] = false;
			}
			e++;
		}
//		for(int i = 0; i < alone_label.size(); i++)
//			System.out.print((events.elementAt(alone_label.elementAt(i)).recognizedAppliance+1) + "-");
//		System.out.println();
		
		try {
			/******************************************* Calculate 0-precondition distances *************************************************/
			for(int i = 0; i < alone_label.size(); i++){
				Impedance appZ = getAppImpedance(conn, events.elementAt(alone_label.elementAt(i)).recognizedAppliance+1);
				//Impedance circuitZ = getCircuitImpedance(conn, events.elementAt(alone_label.elementAt(i)).Timestamp);
				
				
			}
			/********************************************   Update Distance Matrix   ********************************************************/
			
			///Distance Matrix should keep the value of the signature index and operation lasting time. 
			///The smaller the sigIndex is the better; the longer the lasting time is the better.

			
//			if(conn.isClosed())
//			{
//				
//			}
//			else {
//				conn.close();
//				conn=null;
//			}

//		} catch (SQLException sqlEx) {
//			System.out.println("SQL exception");
//			sqlEx.printStackTrace();
//			// log connection fail.
		} catch (Exception e1) {
			e1.printStackTrace();
		}
	
		
		/*  TEST: print out the precondition calculation result.  */
//		for(int p = 0; p < poolSize; p++){
//			Debugger.showln("Cluster#" + (p+1) );
//			for(int q = 0; q < poolSize; q++){
//				Debugger.show((q+1) + " ");
//				for(int r = 0; r < condLog[p].precondition[q]; r++) 
//					Debugger.show("=");
//				Debugger.showln("");
//			}
//			Debugger.showln("");
//			
//		}

	}

		
//	private static Impedance getCircuitImpedance(Connection conn2, String timestamp) {
//		Statement statement = null;
//		ResultSet sqlrs = null;
//		Impedance ret = new Impedance();
//		try {
//			statement = conn.createStatement();
//			String queryAppID = "select * from Energy_Sequence where timestamp = " + timestamp; //get event sequence
//			sqlrs = statement.executeQuery(queryAppID);
//			while (sqlrs.next()) {
//				ret.setR(sqlrs.getFloat("R"));
//				if(sqlrs.getFloat("C") != 0)
//					ret.setX( FREQUENCY*sqlrs.getFloat("L") - (1/(FREQUENCY*sqlrs.getFloat("C"))) );
//				else
//					ret.setX( FREQUENCY*sqlrs.getFloat("L"));
//			}
//			sqlrs.close();
//			statement.close();
//			
//		} catch (SQLException e) {
//			System.out.println("SQL ERROR!!");
//			e.printStackTrace();
//		}
//		
//		return null;
//	}


	private static Impedance getAppImpedance(Connection conn, int appID) {
		Statement statement = null;
		ResultSet sqlrs = null;
		Impedance ret = new Impedance();
		try {
			statement = conn.createStatement();
			String queryAppID = "select * from APP_Template where idx = " + appID; //get event sequence
			sqlrs = statement.executeQuery(queryAppID);
			while (sqlrs.next()) {
				ret.setR(sqlrs.getFloat("R"));
				if(sqlrs.getFloat("C") != 0)
					ret.setX( FREQUENCY*sqlrs.getFloat("L") - (1/(FREQUENCY*sqlrs.getFloat("C"))) );
				else
					ret.setX( FREQUENCY*sqlrs.getFloat("L"));
			}
			sqlrs.close();
			statement.close();
			
		} catch (SQLException e) {
			System.out.println("SQL ERROR!!");
			e.printStackTrace();
		}
		
		
		return ret;
	}


	/**
	 * DEBUG: Prints out current appliance condition.
	 */
	private static void printCurrentAppCondition() {
		for(int c = 0; c < poolSize; c++)
			System.out.print(currentAppCondition[c]+ " ");
		System.out.println();
		
	}

	/**
	 * Prints out the support times for all the precondition for cluster CID
	
	public static void printPrecondition(int CID){
		for(int i = 0; i < condLog[CID].size(); i++){
			for(int j = 0; j < poolSize; j++){
				System.out.print(condLog[CID].elementAt(i).appState[j]);
			}
			System.out.println("\t" + condLog[CID].elementAt(i).eID.size());
		}
		
	} */
	
	/**
	 * type "yyyy-MM-dd HH:mm:ss"
	 * @param time
	 * @return
	 */
	public static String longToTimestampString(long time){
		DateFormat dataFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); // �s��Data
		// base
		// ��timeStamp
		
		return dataFormat.format(time);
	}
}
