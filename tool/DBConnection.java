package tool;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class DBConnection {
	private static Properties props;

	/**
	 * Get properties
	 */
	private static void loadProperties() {
		props = new Properties();
		try {
			props.load(new FileInputStream("./settings/DBconfig.properties"));
		} catch (FileNotFoundException e) {
			System.out.println("Can't find file.");
			System.out.println(new File(".").getAbsolutePath());
		} catch (IOException e) {
			System.out.println(e.toString());
		}
	}

	private static String getConfig(String key) {
		return props.getProperty(key);
	}

	public static void main(String[] args) {
		
		test();
		
	}

	public static void test() {
		loadProperties();
		String driver = getConfig("MySql_driver");
		String url = getConfig("MySql_url");
		String user = getConfig("MySql_user");
		String password = getConfig("MySql_password");

		try {
			Class.forName(driver);
			Connection conn = DriverManager.getConnection(url, user, password);

			if (conn != null && !conn.isClosed()) {

				System.out.println("DataBase connection success!");
				System.out.println("--------JDBC Connection \t--------------");
				System.out.println("--------url:" + url	+ "\t---------------");
				System.out.println("--------username:" + user + "\t---------");
				System.out.println("--------password:" + password + "\t-----");
				conn.close();
			}

		} catch (ClassNotFoundException e) {
			System.out.println("DB driver class name not found.");
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/**
	 * �]�wJDBC connection �s�u
	 * 
	 * @return
	 * @throws Exception
	 */
	public static Connection getConnection() throws Exception {
		// Load the JDBC driver
		loadProperties();
		String driver = getConfig("MySql_driver");
		Class.forName(driver);
		String url = getConfig("MySql_url");
		String username = getConfig("MySql_user");
		String password = getConfig("MySql_password");
		// LogFile.println("--------JDBC Connection \t-----------------");
		// LogFile.println("--------url:"+url+"\t----------------------");
		// LogFile.println("--------username:"+username+"\t------------");
		// LogFile.println("--------password:"+password+"\t------------");
		//System.out.println(url+" "+username+" "+password);
		return DriverManager.getConnection(url, username, password);
	}
}
