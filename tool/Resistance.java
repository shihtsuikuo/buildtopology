package tool;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Resistance {
	/**
	 * CABLE_RESISTANCE is the resistance of the cooper cable per meter.
	 */
	private static float CABLE_RESISTANCE = 0.00547F;
	private static final int AVERAGE_ENTRIES = 3;
	private static Connection conn = null;
	private static Statement stm = null;
	private static ResultSet rs = null;
	
	public static float getDistance_noPreCond(float[] voltage, float[] current, int clusterID, int endIdx ){
		float dist = 0.0F;
		float resistanceSum = 0.0F;
		float templateResistanceAvg = 0.0F;
		int countLength = 0;
		for(int i = 0; i < voltage.length; i++){
			/* check if the voltage/current array are filled correctly, because if the eventlength are too short in main.java, array values might be 0 */
			if(voltage[i] == 0) break; 
			countLength++;
			resistanceSum += voltage[i] / current[i];
		}
		templateResistanceAvg = lookUpResistanceAvg(clusterID, endIdx);
		System.out.println("actualResistanceAvg = " + resistanceSum/countLength);
		dist = ((resistanceSum/countLength) - templateResistanceAvg) / CABLE_RESISTANCE;
		return dist;
	}
	
	private static float lookUpResistanceAvg(int clusterID, int endIdx) {
		float templateResistanceAvg = 0.0F;
		int countLength = 0;
		try {
			conn = DBConnection.getConnection();
			stm = conn.createStatement();
			/* get appliance template data */
			String queryAppID = "select * from APP_Template where AppID = " + CIDtoAppID(clusterID);
			queryAppID = queryAppID + " and Idx > " + (endIdx - AVERAGE_ENTRIES) + " and Idx <= " + endIdx;   /// What if the actual operating length exceed the template length?
			rs = stm.executeQuery(queryAppID);
			while(rs.next()){
				countLength++;
				templateResistanceAvg += (rs.getFloat("voltage") / rs.getFloat("current"));
			}
			templateResistanceAvg = templateResistanceAvg / countLength;
			System.out.println("Cluster #" + clusterID + " lookUpResistanceAvg = " + templateResistanceAvg);
			return templateResistanceAvg;
		} catch (SQLException sqlEx) {
			System.out.println("SQL execution fail.");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
	}

	private static int CIDtoAppID(int clusterID) {
		return (clusterID+1)/2;
	}

}
